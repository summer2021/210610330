//
// Created by liuming on 2021/8/11.
//

#ifndef LITE_HEXAGON_DELEGATE_H
#define LITE_HEXAGON_DELEGATE_H

#include <vector>
#include <map>
#include <include/api/delegate.h>
#include "hexagon_nn/hexagon_nn.h"

namespace mindspore{
    class HexagonDelegate : public Delegate{
    public:
        explicit HexagonDelegate() = default;
        ~HexagonDelegate() override = default;

        int Init() override;

        int Build(DelegateModel *model) override;

    private:

        //like NPU/TensorRT, I need a GetOP function

        //kernel::kernel *

    };
}

#endif //LITE_HEXAGON_DELEGATE_H
