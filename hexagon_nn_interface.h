//
// Created by liuming on 2021/8/12.
//

#ifndef LITE_HEXAGON_NN_INTERFACE_H
#define LITE_HEXAGON_NN_INTERFACE_H
#include "mindspore/lite/src/delegate/Hexagon/hexagon_nn/hexagon_nn.h"

//decltype() is used to get the type of the function name
//[exmaple]
// int itest(){};
// decltype(itest) <=> int

using hexagon_nn_config_fn = decltype(hexagon_nn_config);

using hexagon_nn_init_fn = decltype(hexagon_nn_init);

using hexagon_nn_set_powersave_level_fn =
decltype(hexagon_nn_set_powersave_level);

using hexagon_nn_set_debug_level_fn = decltype(hexagon_nn_set_debug_level);

using hexagon_nn_prepare_fn = decltype(hexagon_nn_prepare);

using hexagon_nn_append_node_fn = decltype(hexagon_nn_append_node);

using hexagon_nn_append_const_node_fn = decltype(hexagon_nn_append_const_node);

using hexagon_nn_execute_fn = decltype(hexagon_nn_execute);

using hexagon_nn_execute_new_fn = decltype(hexagon_nn_execute_new);

using hexagon_nn_teardown_fn = decltype(hexagon_nn_teardown);

using hexagon_nn_snpprint_fn = decltype(hexagon_nn_snpprint);

using hexagon_nn_getlog_fn = decltype(hexagon_nn_getlog);

using hexagon_nn_get_perfinfo_fn = decltype(hexagon_nn_get_perfinfo);

using hexagon_nn_reset_perfinfo_fn = decltype(hexagon_nn_reset_perfinfo);

using hexagon_nn_op_id_to_name_fn = decltype(hexagon_nn_op_id_to_name);

//using hexagon_nn_global_teardown_fn = decltype(hexagon_nn_global_teardown);

//using hexagon_nn_global_init_fn = decltype(hexagon_nn_global_init);

//using hexagon_nn_is_device_supported_fn =
//decltype(hexagon_nn_is_device_supported);

using hexagon_nn_version_fn = decltype(hexagon_nn_version);

//using hexagon_nn_hexagon_interface_version_fn =
//decltype(hexagon_nn_hexagon_interface_version);

#endif //LITE_HEXAGON_NN_INTERFACE_H
