//
// Created by liuming on 2021/8/12.
//
#include "hexagon_implementation.h"
#include "hexagon_nn/hexagon_nn.h"
#include <dlfcn.h>
#include "src/common/log_adapter.h"

namespace mindspore::lite{
    namespace {
        void * LoadFunction(void * dl_handle, const char * name){
            if(dl_handle== nullptr) {
                MS_LOG(ERROR)<<"";
            }
            auto * func_pt = dlsym(dl_handle,name);
            if (func_pt == nullptr){
                MS_LOG(ERROR)<<"Find function failed.";
            }
            return func_pt;
        }

#define LOAD_FUNCTION(dl_handle,method_name,hexagon_obj)                \
    hexagon_obj.method_name = reinterpret_cast<method_name##_fn*>(      \
        LoadFunction(dl_handel,#method_name));                          \
        if((hexagon_obj.method_name)==nullptr){                         \
        successfully_loaded = false;}                                   \

        // Load the shared library "libhexagon_nn_skel.so"
        HexagonNN CreateNewHexagonNnSkel(){
            HexagonNN hexagon_nn;
            void * libhexagon_nn_skel =
                    dlopen("libhexagon_nn_skel.so", RTLD_LAZY | RTLD_LOCAL);
            if(libhexagon_nn_skel == nullptr){
                MS_LOG(ERROR)<<"libhexagon_nn_skel.so loaded failed.";
                return hexagon_nn;
            }
            bool successfully_loaded = true;
            LOAD_FUNCTION(libhexagon_nn_skel,hexagon_nn_config,hexagon_nn);
        }


    }
}